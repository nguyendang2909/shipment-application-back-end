var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/mydb";

MongoClient.connect(url, function(err, db) {
  if (err) throw err;
    var dbo = db.db("ship");
    var myobj = [
        {weight: 250   , price: 12.43     , from: "FR" , to: "FR"},
        {weight: 500   , price: 12.43     , from: "FR" , to: "FR"},
        {weight: 750   , price: 15.42     , from: "FR" , to: "FR"},
        {weight: 1000  , price: 15.42     , from: "FR" , to: "FR"},
        {weight: 2000  , price: 20.77     , from: "FR" , to: "FR"},
        {weight: 3000  , price: 26.07     , from: "FR" , to: "FR"},
        {weight: 4000  , price: 31.43     , from: "FR" , to: "FR"},
        {weight: 5000  , price: 36.77     , from: "FR" , to: "FR"},
        {weight: 6000  , price: 42.13     , from: "FR" , to: "FR"},
        {weight: 7000  , price: 47.49     , from: "FR" , to: "FR"},
        {weight: 8000  , price: 52.83     , from: "FR" , to: "FR"},
        {weight: 9000  , price: 58.83     , from: "FR" , to: "FR"},
        {weight: 10000 , price: 63.54     , from: "FR" , to: "FR"},
        {weight: 11000 , price: 88.19     , from: "FR" , to: "FR"},
        {weight: 12000 , price: 88.19     , from: "FR" , to: "FR"},
        {weight: 13000 , price: 88.19     , from: "FR" , to: "FR"},
        {weight: 14000 , price: 88.19     , from: "FR" , to: "FR"},
        {weight: 15000 , price: 88.19     , from: "FR" , to: "FR"},
        {weight: 99999999999999999999999999999999999 , price: 100       , from: "FR" , to: "FR"},
    ];

    dbo.collection("rate").insertMany(myobj, function(err, res) {
        if (err) throw err;
        console.log("Number of documents inserted: " + res.insertedCount);
        db.close();
        });
      
        

  
});
