const express = require('express');

const app = express();
const convert = require('convert-units');
const { MongoClient } = require('mongodb');

const url = 'mongodb://localhost:27017/';
let pricevalue = 0;
app.use(express.json());


app.use((request, response, next) => {
  response.setHeader('Access-Control-Allow-Origin', '*');
  response.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT');
  response.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');

  next();
});


//function GetQuote
function GetQuote(userData) {
  

}


// Get Quote
app.post('/getQuote', (req, res) => {
  // Origin country, destination country, weight fields are required (weight >=0)
  if (req.body.weight <= 0
    || Number.isNaN(req.body.weight)
    || req.body.sendercountry === ''
    || req.body.receivercountry === '') {
    res.send({ statusGetQuote: false });
  } else {
    req.body.weight = convert(req.body.weight).from('kg').to('g'); // Convert weight from kg to g
    MongoClient.connect(url)
      .then((db) => {
        const myobj = {
          sendername: `${req.body.sendername}`,
          senderemail: `${req.body.senderemail}`,
          senderphone: `${req.body.senderphone}`,
          senderadress: `${req.body.senderadress}`,
          senderlocality: `${req.body.senderlocality}`,
          senderpostalcode: `${req.body.senderpostalcode}`,
          sendercountry: `${req.body.sendercountry}`,
          receivername: `${req.body.receivername}`,
          receiveremail: `${req.body.receiveremail}`,
          receiverphone: `${req.body.receiverphone}`,
          receiveradress: `${req.body.receiveradress}`,
          receiverlocality: `${req.body.receiverlocality}`,
          receiverpostalcode: `${req.body.receiverpostalcode}`,
          receivercountry: `${req.body.receivercountry}`,
          receiverlength: `${req.body.receiverlength}`,
          receiverwidth: `${req.body.receiverwidth}`,
          receiverheight: `${req.body.receiverheight}`,
          weight: `${req.body.weight}`,
        };

        // Write customer information into the database
        db.db('ship').collection('user').insert(myobj).then(() => {
          // console.log(`Number of documents inserted: ${res.insertedCount}`);
        });
        // Find the price based on "rate" table
        db.db('ship').collection('rate').findOne({
          weight: { $gte: req.body.weight },
          from: req.body.sendercountry,
          to: req.body.receivercountry,
        })
          .then((result) => {
            if (!result) {
              res.send({ statusGetQuote: false }); // Notify re-entered if result is null
            } else {
              pricevalue = result.price;
              res.send({ giatien: result.price, statusGetQuote: true });
            }
            // console.log(result.weight);
          });
      })
      .catch((err) => {
        if (err) throw err;
      });
  }
});



//function createShipment

function createShipment(userData,pricevalue){
  
  const ref = Math.floor((Math.random() * 10000000000)); // Random number in 10 character
  const shipmentobj = {
    ref: `${ref}`,
    createdat: `${new Date().getDate()}/${new Date().getMonth()}/${new Date().getFullYear()} ${new Date().getHours()}:${new Date().getMinutes()}:${new Date().getSeconds()}`,
    cost: `${pricevalue}`,
    sendername: `${userData.sendername}`,
    senderemail: `${userData.senderemail}`,
    senderphone: `${userData.senderphone}`,
    senderadress: `${userData.senderadress}`,
    senderlocality: `${userData.senderlocality}`,
    senderpostalcode: `${userData.senderpostalcode}`,
    sendercountry: `${userData.sendercountry}`,
    receivername: `${userData.receivername}`,
    receiveremail: `${userData.receiveremail}`,
    receiverphone: `${userData.receiverphone}`,
    receiveradress: `${userData.receiveradress}`,
    receiverlocality: `${userData.receiverlocality}`,
    receiverpostalcode: `${userData.receiverpostalcode}`,
    receivercountry: `${userData.receivercountry}`,
    receiverlength: `${userData.receiverlength}`,
    receiverwidth: `${userData.receiverwidth}`,
    receiverheight: `${userData.receiverheight}`,
    weight: `${userData.weight}`,

  };

  return new Promise((resolve,reject)=>{
    MongoClient.connect(url)
    .then((db) => {
      db.db('ship').collection('shipment').insert(shipmentobj)
        .then(() => {
          // console.log(`Number of documents inserted: ${res.insertedCount}`);
        });

      resolve({ ref });
    })
    .catch((err) => {
      if (err) throw err;
    });
  })
  

}



// CreateShipment
app.post('/createShipment', (req, shipment) => {
  const userData = req.body;
  
  createShipment(userData,pricevalue)
  .then((result) =>{
    shipment.send(result);
  })
  .catch((err)=>{if (err) throw err;})
});

//Function Get Shipment
function getShipment(refValue) {
  return new Promise((resolve,reject) =>{
    MongoClient.connect(url)
    .then((db)=>{
      db.db('ship').collection('shipment').findOne({ref: refValue})
      .then((result) => {
        if (!result) {resolve({statusGetShipment: false}) }
        else {
          resolve({ resultGetShip: result, statusGetShipment: true })
        }
      })
    })
    .catch((err) => {throw err})
  })
}

// GetShipment
app.post('/getShip', (request, response) => {
  // console.log(req.body);
  const refValue = request.body.trackref;
  getShipment(refValue)
  .then((r) => {
    response.send(r)
  })
  .catch((err) => {throw err})
});






// Function Delete Shipment
function deleteShipment(refValue) {
  return new Promise((resolve, reject) => {
    MongoClient.connect(url)
      .then((db) => {
        db.db('ship').collection('shipment').deleteOne({ ref: refValue })
          .then((result) => {
            db.close();
            resolve(result);
          });
      })
      .catch((err) => {
        throw err;
      });
  });
}


// DeleteShipment
app.post('/deleteShip', (request, response) => {
  
  const refValue = request.body.trackref;
  deleteShipment(refValue)
    .then((r) => {
      response.send(r); 
    })
    .catch((err) => {
      if (err) throw err;
    });
    
    ;
      
});


app.listen(1234, () => {
  //  console.log('Running')
});