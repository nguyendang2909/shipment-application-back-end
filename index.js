const express = require('express');

const app = express();


const { getQuote } = require('./functions.js');
const { createShipment } = require('./functions.js');
const { getShipment } = require('./functions.js');
const { deleteShipment } = require('./functions.js');


let pricevalue = 0;
app.use(express.json());


app.use((request, response, next) => {
  response.setHeader('Access-Control-Allow-Origin', '*');
  response.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT');
  response.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');

  next();
});


// Get Quote
app.post('/getQuote', (request, response) => {
  const userData = request.body;
  getQuote(userData)
    .then((result) => {
      response.send(result);
      pricevalue = result.giatien;
    })
    .catch((err) => {
      if (err) throw err;
    });
});


// CreateShipment
app.post('/createShipment', (req, shipment) => {
  // console.log(req.body);
  const userData = req.body;
  const price = pricevalue;
  createShipment(userData, price)
    .then((result) => {
      shipment.send(result);
      // console.log(result.ref);
    })
    .catch((err) => { if (err) throw err; });
});


// GetShipment
app.post('/getShip', (request, response) => {
  // console.log(request.body);
  const refValue = request.body.trackref;
  getShipment(refValue)
    .then((result) => {
      response.send(result);
    })
    .catch((err) => { throw err; });
});


// DeleteShipment
app.post('/deleteShip', (request, response) => {
  // console.log(request.body);
  const refValue = request.body.trackref;
  deleteShipment(refValue)
    .then((result) => {
      response.send(result);
      // console.log(result.deletedCount);
    })
    .catch((err) => {
      if (err) throw err;
    });
});


app.listen(1234, () => {
  //  console.log('Running')
});
