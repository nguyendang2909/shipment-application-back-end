const { MongoClient } = require('mongodb');

const url = 'mongodb://localhost:27017/';
const convert = require('convert-units');


// function GetQuote
function getQuote(userData) {
  return new Promise((resolve,reject) => {
    // Origin country, destination country, weight fields are required (weight >=0)
    if (userData.weight <= 0
        || Number.isNaN(userData.weight)
        || userData.sendercountry === ''
        || userData.receivercountry === '') {
      resolve({ statusGetQuote: false });
    } else {
      const userweight = convert(userData.weight).from('kg').to('g'); // Convert weight from kg to g
      MongoClient.connect(url)
        .then((db) => {
          // Find the price based on "rate" table
          db.db('ship').collection('rate').findOne({
            weight: { $gte: userweight },
            from: userData.sendercountry,
            to: userData.receivercountry,
          })
            .then((result) => {
              if (!result) {
                resolve({ statusGetQuote: false }); // Notify re-entered if result is null
              } else {
                resolve({ giatien: result.price, statusGetQuote: true });
              }
              // console.log(result.weight);
            });
        })
        .catch((err) => {
          if (err) throw err;
        });
    }
  });
}


// function createShipment

function createShipment(userData, pricevalue) {
  const ref = Math.floor((Math.random() * 10000000000)); // Random number in 10 character
  const shipmentobj = {
    ref: `${ref}`,
    createdat: `${new Date().getDate()}/${new Date().getMonth()}/${new Date().getFullYear()} ${new Date().getHours()}:${new Date().getMinutes()}:${new Date().getSeconds()}`,
    cost: `${pricevalue}`,
    sendername: `${userData.sendername}`,
    senderemail: `${userData.senderemail}`,
    senderphone: `${userData.senderphone}`,
    senderadress: `${userData.senderadress}`,
    senderlocality: `${userData.senderlocality}`,
    senderpostalcode: `${userData.senderpostalcode}`,
    sendercountry: `${userData.sendercountry}`,
    receivername: `${userData.receivername}`,
    receiveremail: `${userData.receiveremail}`,
    receiverphone: `${userData.receiverphone}`,
    receiveradress: `${userData.receiveradress}`,
    receiverlocality: `${userData.receiverlocality}`,
    receiverpostalcode: `${userData.receiverpostalcode}`,
    receivercountry: `${userData.receivercountry}`,
    receiverlength: `${userData.receiverlength}`,
    receiverwidth: `${userData.receiverwidth}`,
    receiverheight: `${userData.receiverheight}`,
    weight: `${userData.weight}`,

  };
  //Write information into database
  return new Promise((resolve,reject) => {
    MongoClient.connect(url)
      .then((db) => {
        db.db('ship').collection('shipment').insertOne(shipmentobj)
          .then(() => {
            // console.log(`Number of documents inserted: ${res.insertedCount}`);
          });

        resolve({ ref });
      })
      .catch((err) => {
        if (err) throw err;
      });
  });
}


// Function Get Shipment
function getShipment(refValue) {
  return new Promise((resolve,reject) => {
    MongoClient.connect(url)
      .then((db) => {
        db.db('ship').collection('shipment').findOne({ ref: refValue })
          .then((result) => {
            if (!result) { resolve({ statusGetShipment: false }); } else {
              resolve({ resultGetShip: result, statusGetShipment: true });
            }
          });
      })
      .catch((err) => { throw err; });
  });
}


// Function Delete Shipment
function deleteShipment(refValue) {
  return new Promise((resolve,reject) => {
    MongoClient.connect(url)
      .then((db) => {
        db.db('ship').collection('shipment').deleteOne({ ref: refValue })
          .then((result) => {
            db.close();
            resolve(result);
          });
      })
      .catch((err) => {
        throw err;
      });
  });
}


module.exports.getQuote = getQuote;
module.exports.createShipment = createShipment;
module.exports.getShipment = getShipment;
module.exports.deleteShipment = deleteShipment;
