const { getQuote } = require('./functions.js');
const { createShipment } = require('./functions.js');
const { getShipment } = require('./functions.js');
const { deleteShipment } = require('./functions.js');

const pr = 10;


const user1 ={ sendername: 'Nguyen Dang Quynh',
  senderemail: 'nguyendang2909@gmail.com',
  senderphone: '0971016191',
  senderadress: 'Hanoi',
  senderlocality: 'Hanoi',
  senderpostalcode: '123456',
  sendercountry: 'FR',
  receivername: 'Nguyen Van An',
  receiveremail: 'nguyenan@gmail.com',
  receiverphone: '0981016191',
  receiveradress: 'Hanoi',
  receiverlocality: 'Hanoi',
  receiverpostalcode: '123456',
  receivercountry: 'FR',
  receiverlength: '1',
  receiverwidth: '1',
  receiverheight: '1',
  weight: '0.1', }

  const user2 ={ sendername: 'Nguyen Dang Quynh',
  senderemail: 'nguyendang2909@gmail.com',
  senderphone: '0971016191',
  senderadress: 'Hanoi',
  senderlocality: 'Hanoi',
  senderpostalcode: '123456',
  sendercountry: 'FR',
  receivername: 'Nguyen Van An',
  receiveremail: 'nguyenan@gmail.com',
  receiverphone: '0981016191',
  receiveradress: 'Hanoi',
  receiverlocality: 'Hanoi',
  receiverpostalcode: '123456',
  receivercountry: 'FR',
  receiverlength: '1',
  receiverwidth: '1',
  receiverheight: '1',
  weight: '1', }

  const user3 ={ sendername: 'Nguyen Dang Quynh',
  senderemail: 'nguyendang2909@gmail.com',
  senderphone: '0971016191',
  senderadress: 'Hanoi',
  senderlocality: 'Hanoi',
  senderpostalcode: '123456',
  sendercountry: 'FR',
  receivername: 'Nguyen Van An',
  receiveremail: 'nguyenan@gmail.com',
  receiverphone: '0981016191',
  receiveradress: 'Hanoi',
  receiverlocality: 'Hanoi',
  receiverpostalcode: '123456',
  receivercountry: 'FR',
  receiverlength: '1',
  receiverwidth: '1',
  receiverheight: '1',
  weight: '2', }

  const user4 ={ sendername: 'Nguyen Dang Quynh',
  senderemail: 'nguyendang2909@gmail.com',
  senderphone: '0971016191',
  senderadress: 'Hanoi',
  senderlocality: 'Hanoi',
  senderpostalcode: '123456',
  sendercountry: 'FR',
  receivername: 'Nguyen Van An',
  receiveremail: 'nguyenan@gmail.com',
  receiverphone: '0981016191',
  receiveradress: 'Hanoi',
  receiverlocality: 'Hanoi',
  receiverpostalcode: '123456',
  receivercountry: 'FR',
  receiverlength: '1',
  receiverwidth: '1',
  receiverheight: '1',
  weight: '3', }

  const user5 ={ sendername: 'Nguyen Dang Quynh',
  senderemail: 'nguyendang2909@gmail.com',
  senderphone: '0971016191',
  senderadress: 'Hanoi',
  senderlocality: 'Hanoi',
  senderpostalcode: '123456',
  sendercountry: 'FR',
  receivername: 'Nguyen Van An',
  receiveremail: 'nguyenan@gmail.com',
  receiverphone: '0981016191',
  receiveradress: 'Hanoi',
  receiverlocality: 'Hanoi',
  receiverpostalcode: '123456',
  receivercountry: 'FR',
  receiverlength: '1',
  receiverwidth: '1',
  receiverheight: '1',
  weight: '17', };

  const user6={ sendername: '',
  senderemail: '',
  senderphone: '',
  senderadress: '',
  senderlocality: '',
  senderpostalcode: '',
  sendercountry: 'FR',
  receivername: '',
  receiveremail: '',
  receiverphone: '',
  receiveradress: '',
  receiverlocality: '',
  receiverpostalcode: '',
  receivercountry: 'FR',
  receiverlength: '',
  receiverwidth: '',
  receiverheight: '',
  weight: '0',
  trackref: '' }

  const user7={ sendername: '',
  senderemail: '',
  senderphone: '',
  senderadress: '',
  senderlocality: '',
  senderpostalcode: '',
  sendercountry: 'FR',
  receivername: '',
  receiveremail: '',
  receiverphone: '',
  receiveradress: '',
  receiverlocality: '',
  receiverpostalcode: '',
  receivercountry: 'FR',
  receiverlength: '',
  receiverwidth: '',
  receiverheight: '',
  weight: '-1',
  trackref: '' }

  const user8 = { sendername: '',
  senderemail: '',
  senderphone: '',
  senderadress: '',
  senderlocality: '',
  senderpostalcode: '',
  sendercountry: 'FR',
  receivername: '',
  receiveremail: '',
  receiverphone: '',
  receiveradress: '',
  receiverlocality: '',
  receiverpostalcode: '',
  receivercountry: 'FR',
  receiverlength: '',
  receiverwidth: '',
  receiverheight: '',
  weight: 'a',
  trackref: '' }

  const user9 = { sendername: '',
  senderemail: '',
  senderphone: '',
  senderadress: '',
  senderlocality: '',
  senderpostalcode: '',
  sendercountry: 'FR',
  receivername: '',
  receiveremail: '',
  receiverphone: '',
  receiveradress: '',
  receiverlocality: '',
  receiverpostalcode: '',
  receivercountry: 'AN',
  receiverlength: '',
  receiverwidth: '',
  receiverheight: '',
  weight: 'FR',
  trackref: '' }

  const user10 = { sendername: '',
  senderemail: '',
  senderphone: '',
  senderadress: '',
  senderlocality: '',
  senderpostalcode: '',
  sendercountry: 'AN',
  receivername: '',
  receiveremail: '',
  receiverphone: '',
  receiveradress: '',
  receiverlocality: '',
  receiverpostalcode: '',
  receivercountry: 'AN',
  receiverlength: '',
  receiverwidth: '',
  receiverheight: '',
  weight: '1',
  trackref: '' }
  
  const user11 = { sendername: '',
  senderemail: '',
  senderphone: '',
  senderadress: '',
  senderlocality: '',
  senderpostalcode: '',
  sendercountry: '',
  receivername: '',
  receiveremail: '',
  receiverphone: '',
  receiveradress: '',
  receiverlocality: '',
  receiverpostalcode: '',
  receivercountry: '',
  receiverlength: '',
  receiverwidth: '',
  receiverheight: '',
  weight: '',
  trackref: '' }

  const user12 ={ sendername: '',
  senderemail: '',
  senderphone: '',
  senderadress: '',
  senderlocality: '',
  senderpostalcode: '',
  sendercountry: 'FR',
  receivername: '',
  receiveremail: '',
  receiverphone: '',
  receiveradress: '',
  receiverlocality: '',
  receiverpostalcode: '',
  receivercountry: 'FR',
  receiverlength: '',
  receiverwidth: '',
  receiverheight: '',
  weight: '1',
  trackref: '3382986693' }

  user13 = { sendername: '',
  senderemail: '',
  senderphone: '',
  senderadress: '',
  senderlocality: '',
  senderpostalcode: '',
  sendercountry: 'FR',
  receivername: '',
  receiveremail: '',
  receiverphone: '',
  receiveradress: '',
  receiverlocality: '',
  receiverpostalcode: '',
  receivercountry: 'FR',
  receiverlength: '',
  receiverwidth: '',
  receiverheight: '',
  weight: '100',
  trackref: '8825446638' }

  user14 = { sendername: '',
  senderemail: '',
  senderphone: '',
  senderadress: '',
  senderlocality: '',
  senderpostalcode: '',
  sendercountry: 'FR',
  receivername: '',
  receiveremail: '',
  receiverphone: '',
  receiveradress: '',
  receiverlocality: '',
  receiverpostalcode: '',
  receivercountry: 'FR',
  receiverlength: '',
  receiverwidth: '',
  receiverheight: '',
  weight: 'a',
  trackref: '8825446638' }
        
describe('Get Quote', () => {
    test('should exist', () => {
      expect(getQuote).toBe(getQuote);
    });
  
    test('Get Quote', () => getQuote(user1).then((res) => {
      expect(res.giatien).toBe(12.43);
    }));
  
    test('Get Quote', () => getQuote(user2).then((res) => {
      expect(res.giatien).toBe(15.42);
    }));
  
    test('Get Quote', () => getQuote(user3).then((res) => {
      expect(res.giatien).toBe(20.77);
    }));
  
    test('Get Quote', () => getQuote(user4).then((res) => {
      expect(res.giatien).toBe(26.07);
    }));
  
    test('Get Quote', () => getQuote(user5).then((res) => {
      expect(res.giatien).toBe(100);
    }));
  
    
    test('Get Quote', () => getQuote(user6).then((res) => {
      expect(res.statusGetQuote).toBe(false);
    }));
  
    test('Get Quote', () => getQuote(user7).then((res) => {
      expect(res.statusGetQuote).toBe(false);
    }));

    test('Get Quote', () => getQuote(user8).then((res) => {
      expect(res.statusGetQuote).toBe(false);
    }));

    test('Get Quote', () => getQuote(user9).then((res) => {
      expect(res.statusGetQuote).toBe(false);
    }));

    test('Get Quote', () => getQuote(user10).then((res) => {
      expect(res.statusGetQuote).toBe(false);
    }));
  
    test('Get Quote', () => getQuote(user11).then((res) => {
      expect(res.statusGetQuote).toBe(false);
    }));

    test('Get Quote', () => getQuote(user14).then((res) => {
      expect(res.giatien).toBeUndefined;
    }));
    
  
  });


  describe('Create Shipment', () => {
    test('should exist', () => {
      expect(createShipment).toBeDefined();
    });
  
    test('Create Shipment', () => createShipment(user1,pricevalue=1).then((res) => {
      expect(res).toHaveProperty('ref');
    }).catch((err) => {
        expect(err).toEqual(Error());
    }));

    test('Create Shipment', () => createShipment(user2,pricevalue=10).then((res) => {
      expect(res.ref).toBeDefined();
    }));
  });

  
  describe('Get Shipment', () => {
    test('Create Shipment', () => getShipment(2310890843).then((res) => {
      expect(res.ref).toBeUndefined();
    }));

    test('Get Shipment', () => getShipment(2310890843).then((res) => {
      expect(res.ref).toBeUndefined();
    }));

    test('Get Shipment', () => {
      createShipment(user1,pricevalue='1').then((response) => {
        const refval = response.ref;
        return getShipment(refval).then((res) => {
          expect(res.ref).toBe(0);
        });
      });
    });

  
  
  })

  describe('Delete Shipment', () => {
    test('should exist', () => {
      expect(deleteShipment).toBeDefined();
    });

    test('Delete Shipment', () => deleteShipment(2310890843).then((res) => {
      expect(res.ref).toBeUndefined();
    }));

    test('Delete Shipment', () => {
      createShipment(user1,pr).then((result) => {
        const ref_num = result.ref;
        return deleteShipment(ref_num).then((res) => {
          expect(res.ref).toBe('1');
        });
      });
    });
  


  })







  